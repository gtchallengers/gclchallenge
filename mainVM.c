/**
	GCLC
*/
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h> /*close*/
#include <netdb.h> /*gethostbyname*/
#include <sys/time.h>
#include <sys/types.h>
#include <string.h>

#define INVALID_SOCKET -1
#define SOCKET_ERROR -1
#define closesocket(s) close(s)
typedef int SOCKET;
typedef struct sockaddr_in SOCKADDR_IN;
typedef struct sockaddr SOCKADDR;
typedef struct in_addr IN_ADDR;

#define TAILLE_NOM 64

//#include "miniz/miniz.c"

static void ReplaceStringKeys(char *buffer, const int lenght);


int main(int argc, char** argv){
    int PORT=15000;
    int sock_err;
    int r;
    char *name;
    char buffer[1024];
    int bool_begin = 1;
	while(bool_begin)
    {
        SOCKET sock;
        /*sock = socket(AF_INET , SOCK_STREAM , 0 );*/
        SOCKADDR_IN sin;
        socklen_t recsize = sizeof(sin);
        sock = socket(AF_INET , SOCK_STREAM , 0 );
	int true = 1;
	setsockopt(sock,SOL_SOCKET,SO_REUSEADDR,&true,sizeof(int));
        if(sock == INVALID_SOCKET)
        {
            printf("socket()\n");
            exit(-1);
        }

        SOCKET csock;
        SOCKADDR_IN csin;
        socklen_t crecsize = sizeof(csin);

        sin.sin_addr.s_addr = htonl(INADDR_ANY); /* Adresse IP automatique */
        sin.sin_port = htons(PORT); /* Listage du port */
        sin.sin_family = AF_INET; /* Protocole familial (IP) */

        sock_err = bind(sock, (SOCKADDR*)&sin, recsize);
        sock_err = listen(sock, 5);

        name = (char *) malloc(TAILLE_NOM);
        r = gethostname(name,(size_t) TAILLE_NOM);
        printf("Serveur sur %s initialise sur le PORT %d !\n", name, PORT);
        free(name);

        csock = accept(sock, (SOCKADDR*)&csin, &crecsize);
        printf("Nouvelle connexion\n");
	//réception de la taille originale
        //memset (buffer, 0, sizeof (buffer));
        //sock_err=recv(csock, buffer, 36, 0);
	//uLong uncomp_len = atoi(buffer)
	//printf("Taille originale reçue : %d\n",uncomp_len); 
        //memset (buffer, 0, sizeof (buffer));
        //sock_err=recv(csock, buffer, 36, 0);
	//uLong src_len = atoi(buffer);
	//printf("Taille compressée reçue : %d\n",src_len); 
        
	
	
	//struct timeval tval_before, tval_after, tval_result;
	//gettimeofday(&tval_before, NULL);
	int cpt =1;
	while(sock_err!=SOCKET_ERROR)
        {
	
	//gettimeofday(&tval_after, NULL);
	//timersub(&tval_after, &tval_before, &tval_result);

	//printf("Time elapsed: %ld.%06ld\n", (long int)tval_result.tv_sec, (long int)tval_result.tv_usec);
 	//réception de la taille originale
        memset (buffer, 36, sizeof (buffer));
	//buffer = calloc( 4 , sizeof(char) );
        sock_err=recv(csock, buffer, 36, 0);
        //uLong uncomp_len = atoi(buffer);
        int uncomp_len = atoi(buffer);
	printf("Taille originale reçue : %d\n",uncomp_len); 
        memset (buffer, 1024, sizeof (buffer));
        sock_err=recv(csock, buffer, 1024, 0);
        //uLong src_len = atoi(buffer);
        //printf("Taille compressée reçue : %d\n",src_len); 

	//réception du fichier compréssé
	//memset (buffer, 0, sizeof (buffer));

	//buffer = realloc(buffer, uncomp_len * sizeof(char));
	
      	//sock_err=recv(csock, buffer, uncomp_len, 0);

        if(sock_err==0) sock_err = -1;
       	if(buffer[0]!=0)
            {
		//printf("Chaine Recu : %s\n", buffer); 
		//int var = fwrite(buffer, 1, sock_err, fichier);
		//printf("%s", var);
		//char* buf = malloc(1048);
 		
		FILE* fichier = NULL;
		if(cpt==1)
		{
			fichier = fopen("/opt/gclc/gclc.log", "wb+"); 
			cpt = 0 ;
		}
		else
			fichier = fopen("/opt/gclc/gclc.log", "ab");
		
		if (!fichier)
			printf("Pas de fichier\n");
		else{
			

			ReplaceStringKeys(buffer, uncomp_len);	
			
			fputs(buffer, fichier);
		
			fclose(fichier);
		}

		//sprintf(buf, "echo \"%s\" >> /opt/gclc/gclc.log", buffer);
		//system(buf);
		//free(buf);
		/*/printf("Taille reçue : %d\n",src_len); 
                
                //unsigned char *pUnCmp = (mz_uint8 *)calloc(1,(size_t)uncomp_len);
                //int cmp_status = uncompress(pUnCmp, &uncomp_len, (const unsigned char *)buffer, src_len);
                if (cmp_status != Z_OK)
                {
                      //printf("uncompress() failed!\n");
                      //free(pUnCmp);
                      //return EXIT_FAILURE;
                }
                //printf("UnCompressed from %u to %u bytes\n", (mz_uint32)src_len, (mz_uint32)uncomp_len);

                //printf("UnCompressed chaine: %s\n",pUnCmp);
		*/

            }

            if(sock_err!=-1){



            }
            //shutdown(csock, 2);
            //closesocket(csock);
        }
	
        printf("Fin de connexion\n");
        shutdown(csock, 2);
        closesocket(csock);
        shutdown(sock, 2);
        closesocket(sock);
    }


	return 0;
}

static void ReplaceStringKeys(char *buffer, const int length)
{
	unsigned int newLength = length;
	unsigned char buf[5];
	memset(buf, '\0', 4); //ptr, val, num
	buf[0] = '#'; // First #
	buf[1] = '0';
	
	int i = 0;

	for (i = 0; i < length; i++)
	{
		if ( ((unsigned char)buffer[i]) < 32 &&  buffer[i] >= 0  && i < length -1 && buffer[i] != '\n' && buffer[i] != '\t')
		{
			int val = buffer[i];
			printf("%d,%c,%o--",val,val,val);
			// dst, src, length
			memmove(buffer + i + 3, buffer + i, length);
			sprintf(buf+2, "%o", val); // target, "format", data
			int intLen = strlen(buf);
			memmove(buffer + i, buf, intLen);
			newLength += 3;
		}
		else if(buffer[i] == '\n'){
			memmove(buffer + i + 3, buffer + i, length);
			sprintf(buf+2, "%o", 10); // target, "format", data
			int intLen = strlen(buf);
			memmove(buffer + i, buf, intLen);
			newLength += 3;
		}
		
		else if(buffer[i] == '\t'){
			memmove(buffer + i + 3, buffer + i, length);
			sprintf(buf+2, "%o", 9); // target, "format", data
			int intLen = strlen(buf);
			memmove(buffer + i, buf, intLen);
			newLength += 3;
		}
		// Convert our magic character 141 to \n to separate the log messages approriately.
		else if (((unsigned char)buffer[i]) == 141)
		{
			buffer[i] = '\n';
		}
	}
	
}
