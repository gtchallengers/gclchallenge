#include<stdio.h> //printf
#include<string.h> //memset
#include<stdlib.h> //exit(0);
#include<arpa/inet.h>
#include<unistd.h>
#include<sys/socket.h>
#include<time.h>
#include <netdb.h> /*gethostbyname*/
#include <errno.h>

#define BUFLEN 1024  //Max length of buffer
#define PORT 514   //The port on which to listen for incoming data
#define INVALID_SOCKET -1
#define SOCKET_ERROR -1

typedef int SOCKET;
typedef struct sockaddr_in SOCKADDR_IN;
typedef struct sockaddr SOCKADDR;
typedef struct in_addr IN_ADDR;


void die(char *s)
{
    perror(s);
    exit(1);
}

int main()
{
    int mv_sock_err;
	time_t rawtime;
  	struct tm * timeinfo;

    struct sockaddr_in si_me, si_other;

	//Timeout receive operations
	struct timeval tv;
	tv.tv_sec = 60;
	tv.tv_usec = 0;

    while(1){

        int s, slen = sizeof(si_other) , recv_len;

        //create a UDP socket
        if ((s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
        {
            die("socket");
        }
        setsockopt(s, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv, sizeof(struct timeval));
	int true = 1;
        setsockopt(s,SOL_SOCKET,SO_REUSEADDR,&true,sizeof(int));

        // zero out the structure
        memset((char *) &si_me, 0, sizeof(si_me));

        si_me.sin_family = AF_INET;
        si_me.sin_port = htons(PORT);
        si_me.sin_addr.s_addr = htonl(INADDR_ANY);

        //bind socket to port
        if( bind(s , (struct sockaddr*)&si_me, sizeof(si_me) ) == -1)
        {
            die("bind");
        }
        //initialisation des variables pour la socket socket d'envoi
        SOCKET mv_sock;
        SOCKADDR_IN mv_sin;
        //socklen_t mv_recsize = sizeof(mv_sin);
        mv_sin.sin_addr.s_addr = inet_addr("51.255.62.53");
        mv_sin.sin_family = AF_INET;/* type */
        mv_sin.sin_port = htons(15000);/*port */
        if ( mv_sin.sin_addr.s_addr == INADDR_NONE ) {
            printf("bad address.\n");
            exit(-1);
         }
        mv_sock = socket(AF_INET, SOCK_STREAM,0);
        int true2 = 1;
        setsockopt(mv_sock,SOL_SOCKET,SO_REUSEADDR,&true2,sizeof(int));
        if(mv_sock == INVALID_SOCKET)
        {
            printf("Virtual Machine socket()\n");
            exit(-1);
        }
        mv_sock_err=connect(mv_sock, (SOCKADDR*)&mv_sin, sizeof(mv_sin));
        if(mv_sock_err == SOCKET_ERROR){
            printf("Echec connexion à la VM\n");
            exit(-1);
        }

        printf("Connexion à la VM réussie. %d\n",mv_sock_err);

        //keep listening for data
        int premier = 1;
        while  ((mv_sock_err != INVALID_SOCKET))
        {

            printf("Waiting for data...\n");
            fflush(stdout);

            static char buf[BUFLEN] = {0};
            memset(buf,0,BUFLEN);
            //try to receive some data, this is a blocking call
            errno = 0;
            if ((recv_len = recvfrom(s, buf, BUFLEN, 0, (struct sockaddr *) &si_other, &slen)) == -1)
            {
                printf("recvfrom()[%d]: La fonction s'est arretée après un long temps d'attente ou a simplement écouhée.\n",errno);
            }

            //si on a passé le 1er tour et le timeout de la réception est atteint
            if (premier == 0 && (recv_len == 0 || errno == EAGAIN || errno == EWOULDBLOCK) ){
                break;
            }
            //si on est au 1er tour et le timeout de la réception est atteint
            if (premier == 1 && (recv_len == 0 || errno == EAGAIN || errno == EWOULDBLOCK) ){
                continue;
            }

            premier = 0 ;

            time (&rawtime);
            timeinfo = localtime (&rawtime);

           //seconds = rawtime - time_start;
            printf("Traitement du buffer ...\n");
            static char buftime[50] = {0}, buf2[1024] = {0} ;
            memset(buftime,0,50);
            memset(buf2,0,1024);
            strftime (buftime,50,"%b %e %T ",timeinfo);
            sprintf(buf2, "%s%s", buftime, buf);

            static char response[10];
            //On envoie à la machine virtuelle le message compressé
            memset(response,0,10);
            sprintf(response,"%d",recv_len);
            printf("Taille originale : %d\n",recv_len);
            mv_sock_err = send(mv_sock,response,10,0);
            
            
            //memset(response,0,36);
            //sprintf(response,"%d",cmp_len);
            //mv_sock_err = send(mv_sock,response,36,0);

            mv_sock_err = send(mv_sock,buf2,strlen(buf2),0);

        }
        close(mv_sock);
        close(s);
    }

    return 0;
}
